VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RegistryRead"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function Registry_Read(Key_Path, Key_Name) As Variant
On Error Resume Next
    Dim registryReaderObj As RegistryReader.RegistryRead
    Set registryReaderObj = CreateObject("RegistryReader.RegistryRead")
    Registry_Read = registryReaderObj.Registry_Read(Key_Path, Key_Name)
End Function

