VERSION 5.00
Begin VB.Form frmPrincipal 
   Caption         =   "Test"
   ClientHeight    =   3405
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6480
   LinkTopic       =   "Form1"
   ScaleHeight     =   3405
   ScaleWidth      =   6480
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmmReadRegistry 
      Caption         =   "Read Registry"
      Height          =   615
      Left            =   3720
      TabIndex        =   4
      Top             =   240
      Width           =   2295
   End
   Begin VB.CommandButton btnSum 
      Caption         =   "Sum"
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   1920
      Width           =   2655
   End
   Begin VB.TextBox tbNumberB 
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Text            =   "0"
      Top             =   1080
      Width           =   2655
   End
   Begin VB.TextBox tbNumberA 
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Text            =   "0"
      Top             =   240
      Width           =   2655
   End
   Begin VB.Label lbRegistryMsg 
      Alignment       =   2  'Center
      Height          =   615
      Left            =   3600
      TabIndex        =   5
      Top             =   1080
      Width           =   2535
   End
   Begin VB.Label lblResult 
      Alignment       =   2  'Center
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   3
      Top             =   2520
      Width           =   2655
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnSum_Click()
    Dim mathObj As MathDll.Math
    Set mathObj = CreateObject("mathDll.Math")
    lblResult.Caption = mathObj.Sum(Val(tbNumberA.Text), Val(tbNumberB.Text))
End Sub

Private Sub cmmReadRegistry_Click()
    Dim registryReaderObj As RegistryReaderDll.RegistryRead
    Set registryReaderObj = CreateObject("RegistryReaderDll.RegistryRead")
    lbRegistryMsg.Caption = registryReaderObj.Registry_Read("HKEY_LOCAL_MACHINE\SOFTWARE\Example\", "Message")
End Sub

