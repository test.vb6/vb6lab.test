$ErrorActionPreference = "Stop"

$currentDirectory    = (Get-Item -Path ".\").FullName
$prj                 = "C:\devel\vb6.gitlab.test"
$srcDir              = "$prj\src"
$buildOutput         = "$srcDir\build\"

# Build 
$vb6bin              = "C:\Program Files\Microsoft Visual Studio\VB98\VB6.EXE"
$buildLog            = "C:\buildExample.log"

# Wizard
$wizardBin           = "C:\Program Files\Microsoft Visual Studio\VB98\Wizards\PDWizard\PDCMDLN.exe"
$wizardOutput        = "C:\OutputExample\package"
$wizardScriptName    = "PackageScript"
$wizardLog           = "C:\wizardExample.log"

# InnoSetup - Package
$innoSetupBin        = "C:\Program Files\Inno Setup 5\ISCC.exe"
$packageLog          = "C:\packageExample.log"
$packageOutput       = "C:\setupExampleGUI"
$packageScript       = "$prj\package.iss"

# Projects 
$mathVbp              = "$srcDir\Math\Math.vbp"
$registryReaderVbp    = "$srcDir\RegistryReader\RegistryReader.vbp"
$registryReaderDllVbp = "$srcDir\RegistryReaderDll\RegistryReaderDll.vbp"
$exampleGUIVbp        = "$srcDir\GUI\ExampleGUI.vbp"

function PackageExe ($script) { 
    Write-Host "Packaging $vbp from InnoSetup"
    try {
        & $innoSetupBin /Q $script 
    } catch {  
        throw "Unable to packaging $vbp from InnoSetup"
    } 
    Write-Host "Packaging... [OK]" -ForegroundColor Green
}

function WizardPackageVbp ($vbp, $scriptName, $log) {
    $failed = $false
    $retries = 0
    $succeeded = $false
    
    Write-Host "Packaging $vbp from wizard"
    try {
        & $wizardBin $vbp /p $scriptName /l $log 
    } catch { 
        $failed = $true
    }
    
    while (!($failed -or $succeeded)) {
        Write-Host -NoNewline "."
        Start-Sleep -s 1
        $failed = HasFailed($log)
        $succeeded = HasSucceeded($log)
        $retries = $retries + 1
        $failed = ($failed -or ($retries -eq 60)) -and !$succeeded
    }
    
    if ($failed)
    {
        $logContent = Get-Content -Path $log 
        Write-Host $logContent -ForegroundColor DarkRed
        throw "Unable to Packaging $vbp from wizard"
    }
    Write-Host "Packaging... [OK]" -ForegroundColor Green
}

function MakeProjectVbp ($vbp, $outdir, $log) {
    $failed = $false
    $retries = 0
    $succeeded = $false
    
    Write-Host "Building $vbp"
    try {
        & $vb6bin /m $vbp /out $log /outdir $outdir 
    } catch { 
        $failed = $true
    }
    
    while (!($failed -or $succeeded)) {
        Write-Host -NoNewline "."
        Start-Sleep -s 1
        $failed = HasFailed($log)
        $succeeded = HasSucceeded($log)
        $retries = $retries + 1
        $failed = ($failed -or ($retries -eq 60)) -and !$succeeded
    }
    
    if ($failed)
    {
        $logContent = Get-Content -Path $log 
        Write-Host $logContent -ForegroundColor DarkRed
        throw "Unable to build $vbp"
    }
    Write-Host "Build... [OK]" -ForegroundColor Green
}

function CreateFile($file) {
    if (Test-Path $file)
    {
        RemoveFile $file
    }
    New-Item -Path $file -Type file -Force | Out-Null
}

function CreateDirectory($dir) {
    if (Test-Path $dir)
    {
        RemoveDirectory $dir
    }
    New-Item -ItemType directory -Path $dir| Out-Null
}
 
function RemoveFile($file) {
    Remove-Item $file -erroraction 'silentlycontinue' | Out-Null
}

function RemoveDirectory($dir) {
    Remove-Item $dir -Force -Recurse -erroraction 'silentlycontinue' | Out-Null
}

function CloneDirectory($src, $dest) {
    Copy-Item $src -Destination $dest -Recurse -erroraction 'silentlycontinue'| Out-Null
}
 
function HasFailed($file) {
    return ((Select-String "failed" $file -Quiet) -or (Select-String "not found" $file -Quiet))
}

function HasSucceeded($logFile) {
    return (Select-String "succeeded" $logFile -Quiet) -or (Select-String "successfully" $logFile -Quiet)
}

function RemovePackageFiles {
    RemoveFile $wizardOutput"\setup.exe"
    RemoveFile $wizardOutput"\SETUP.LST"
    RemoveFile $wizardOutput"\*.CAB"
    RemoveFile $wizardOutput"\Support\*.bat"
    RemoveFile $wizardOutput"\Support\*.DDF"
    RemoveFile $wizardOutput"\Support\MDAC_TYP.EXE"
    RemoveFile $wizardOutput"\Support\SETUP.EXE"
    RemoveFile $wizardOutput"\Support\Setup.Lst"
    RemoveFile $wizardOutput"\Support\SETUP1.EXE"
    RemoveFile $wizardOutput"\Support\ST6UNST.EXE"
    RemoveFile $wizardOutput"\Support\VB6STKIT.DLL"
    RemoveFile $wizardOutput"\Support\COMCAT.DLL"
    RemoveFile $wizardOutput"\Support\MSVCRT40.DLL"
    RemoveFile $wizardOutput"\Support\STDOLE2.TLB"
    RemoveFile $wizardOutput"\Support\ASYCFILT.DLL"
    RemoveFile $wizardOutput"\Support\OLEPRO32.DLL"
    RemoveFile $wizardOutput"\Support\OLEAUT32.DLL"
    RemoveFile $wizardOutput"\Support\msvbvm60.dll"
    RemoveFile $wizardOutput"\Support\wininet.dll"
}

# Elimina codigo fuente actual
RemoveDirectory   $prj

# Copia el codigo fuente de git a la carpeta donde se va a compilar
CloneDirectory    $currentDirectory   $prj

# Campia de directorio 
CD $prj 

# Genera archivos y directorios necesarios
CreateDirectory   $wizardOutput
CreateDirectory   $packageOutput
CreateFile        $buildLog
CreateFile        $wizardLog
CreateFile        $packageLog

# Compilar proyectos .vbp
MakeProjectVbp $mathVbp               $buildOutput   $buildLog
MakeProjectVbp $registryReaderVbp     $buildOutput   $buildLog
MakeProjectVbp $registryReaderDllVbp  $buildOutput   $buildLog
MakeProjectVbp $exampleGUIVbp         $buildOutput   $buildLog

# Empaquetar con wizard de VB6
WizardPackageVbp $exampleGUIVbp $wizardScriptName $wizardLog

# Eliminar archivos inecesarios
RemovePackageFiles

# Empaquetar con InnoSetup para obtener el .exe
PackageExe $packageScript