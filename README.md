# GitLab CI & VB6
Proyecto de ejemplo para configuración de GitLab CI & VB6.
 
## Estructura del proyecto
 
```mermaid
graph LR
 
A((ExampleGUI.vbp))--> B((Math.vbp))
D((RegistryReaderDll.vbp)) --> C((RegistryReader.vbp))
A((ExampleGUI.vbp)) --> D((RegistryReaderDll.vbp)) 
